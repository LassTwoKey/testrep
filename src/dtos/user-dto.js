export default class UserDto {
	id;
	name;
	constructor(model) {
		this.id = model.id;
		this.name = model.name;
	}
}