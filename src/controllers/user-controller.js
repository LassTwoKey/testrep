import pool from '../db.js';
import bcrypt from 'bcrypt';
import { jwtTokens } from '../utils/jwt-helpers.js';
import { getUsers, addUser, getUserById } from '../queries/users-queries.js';

class UserController {
	async getUsers(req, res) {
		// try {
		// 	console.log(req.cookies)
		// 	pool.query(getUsers, (error, result) => {
		// 		if (error) throw error
		// 		return res.status(200).json(result.rows)
		// 	})
		// } catch (error) {
		// 	return res.status(500).json({ error: error.message })
		// }
		pool.query(getUserById, [userID], (error, result) => {
			if (error) throw error

			console.log(result.rows)
		})
	}
	async addUser(req, res) {
		try {
			// const { name, password } = req.body
			// const hashedPassword = await bcrypt.hash(password, 5)
			// pool.query(checkNameExists, [name], (error, result) => {
			// 	if (result.rows) {
			// 		if (error) throw error
			// 		return res.send("Пользователь уже существует")
			// 	}

			// 	pool.query(addUser, [name, hashedPassword], (error)=> {
			// 		if (error) throw error
			// 		return res.status(201).send("Успешная регистрация")
			// 	})
			// })

			// const newUser = pool.query(addUser, [name, hashedPassword], (error, result) => {
			// 	if (error) throw error
			// 	res.status(201).send("Success registration")
			// });
			// res.json(jwtTokens(newUser.rows[0]))
		} catch (error) {
			console.log(121);
			res.status(500).json({ error: error.message });
		}
		try {
			const hashedPassword = await bcrypt.hash(req.body.password, 10);
			const newUser = await pool.query(addUser, [req.body.name, req.body.password, hashedPassword]);
			addUser
			res.json(jwtTokens(newUser.rows[0]));
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async deleteUser(req, res) {
		try {
			const users = await pool.query('DELETE FROM users');
			res.status(204).json(users.rows);
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
}
export default new UserController()