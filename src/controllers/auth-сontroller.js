
import { signupService } from "../services/auth-service.js";

class AuthController {
	async registration(req, res) {
		try {
			await signupService(req, res)
		} catch (error) {
			return res.status(400).json({ message: "Registration error" })
		}
	}
	async login(req, res) {
		try {

		} catch (error) {
			return res.status(400).json({ message: "Login error" })
		}
	}
}

export default new AuthController()
