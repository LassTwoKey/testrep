import express from 'express';
import { authenticateToken } from '../middleware/authorization.js';
import userController from '../controllers/user-controller.js';

import pool from '../db.js';
import bcrypt from 'bcrypt';

let refreshTokens = [];

const router = express.Router();

router.get('/users', userController.getUsers)
router.post('/users', userController.addUser);
router.delete('/users', userController.deleteUser)


export default router;