export const getUsers = 'SELECT * FROM users'
export const getUserById = 'SELECT u FROM users u WHERE u.id = $1'
export const addUser = 'INSERT INTO users (name,password) VALUES ($1,$2) RETURNING *'
export const addToken = 'INSERT INTO tokens (id,refresh_token) VALUES ($1,$2) RETURNING *'
export const getUserByName = 'SELECT * FROM users WHERE name = $1'
// export const getUserByName = 'SELECT * FROM users WHERE name = $1'


//export const getUserByName = 'SELECT u FROM users u WHERE u.name = $1'

