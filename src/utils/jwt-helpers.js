import jwt from 'jsonwebtoken';

//Generate an access token and a refresh token for this database user
function jwtTokens({ user_id, user_name, user_email }) {
	const user = { user_id, user_name, user_email };
	const accessToken = jwt.sign(user, process.env.JWT_ACCESS_SECRET, { expiresIn: '15m' });
	const refreshToken = jwt.sign(user, process.env.JWT_REFRESH_SECRET, { expiresIn: '30d' });
	return ({ accessToken, refreshToken });
}

export { jwtTokens };