import Pool from 'pg'

const DBPool = Pool.Pool

const pool = new DBPool({
	user: 'postgres',
	host: 'localhost',
	database: 'jwt_authorization',
	port: '5432',
	password: '1',
})

pool.connect(function (err) {
	if (err) throw err;
	console.log("Connected!");
});

export default pool
