import jwt from 'jsonwebtoken'

export const generateTokens = async (payload) => {
	const accessToken = jwt.sign(payload, process.env.JWT_ACCESS_SECRET, {
		expiresIn: '1h'
	});
	const refreshToken = jwt.sign(payload, process.env.JWT_REFRESH_SECRET, {
		expiresIn: '30d'
	});

	return {
		accessToken,
		refreshToken
	}
}

export const saveToken = async (userID, refreshToken) => {
	// const tokenData = await AuthToken.findOne({
	// 	user: userID
	// })
	// if (userID) {
	// 	tokenData.refreshToken = refreshToken;
	// 	return tokenData.update();
	// }

	// const token = await AuthToken.create({ user: userID, refreshToken });

	pool.query(getUserById, [userID], (error, result) => {
		if (error) throw error

		console.log(result.rows)
	})
	return token
}
