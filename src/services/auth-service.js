import pool from "../db.js";
import bcrypt from 'bcrypt';
import { getUserByName, addToken, addUser } from "../queries/users-queries.js";
import { generateTokens } from "./token-service.js";
import UserDto from "../dtos/user-dto.js";


export const signupService = async (req, res) => {
	const { name, password } = req.body
	const hashedPassword = await bcrypt.hash(password, 5)
	pool.query(getUserByName, [name], async (error, result) => {
		if (result.rows.length) {
			if (error) throw error
			return res.send(`Пользователь ${name} уже существует`)
		}

		const userQuery = await pool.query(addUser, [name, hashedPassword])
		const userDto = new UserDto(userQuery.rows[0])
		const tokens = await generateTokens({ ...userDto })
		pool.query(addToken, [userDto.id, tokens.refreshToken], async (error, result) => {
			return res.status(201).send("Успешная регистрация")
		})

	})
}