import express, { json } from 'express'
import cors from 'cors'
import usersRouter from './routes/users-routes.js'
import authRouter from './routes/auth-routes.js'
import bodyParser from "body-parser";
import dotenv from 'dotenv'
import cookieParser from 'cookie-parser'

dotenv.config();
const PORT = process.env.PORT || 6542;

const app = express();

const corsOptions = { credentials: true, origin: process.env.URL || '*' };

app.use(cors(corsOptions))
app.use(json())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser())

app.use('/api', authRouter);
app.use('/api', usersRouter);

const start = async () => {
	try {
		app.listen(PORT, () => {
			console.log(`Server is listening on port:${PORT}`);
		})
	} catch (error) {
		console.log(error)
	}
}

start()

